/*
 *  Board-specific setup code for the AT91SAM9x5 Evaluation Kit family
 *
 *  Copyright (C) 2010 Atmel Corporation.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 */

#include <linux/types.h>
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/spi/flash.h>
#include <linux/spi/spi.h>
#include <linux/fb.h>
#include <linux/gpio_keys.h>
#include <linux/input.h>
#include <linux/leds.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <mach/cpu.h>
#include <mtd/mtd-abi.h>

#include <video/atmel_lcdfb.h>
#include <media/soc_camera.h>
#include <media/atmel-isi.h>

#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/irq.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>

#include <mach/hardware.h>
#include <mach/board.h>
#include <mach/gpio.h>
#include <mach/atmel_hlcdc.h>
#include <mach/at91sam9_smc.h>
#include <mach/at91_shdwc.h>

#include "sam9_smc.h"
#include "generic.h"
#include <mach/board-sam9x5.h>

static void __init nbfx100_map_io(void)
{
	/* Initialize processor: 12.000 MHz crystal */
	at91sam9x5_initialize(12000000);

	/* DGBU on ttyS0. (Rx & Tx only) */
	at91_register_uart(0, 0, 0);
	at91_register_uart(AT91SAM9X5_ID_UART0, 1, 0);
	at91_register_uart(AT91SAM9X5_ID_UART1, 2, 0);

	/* set serial console to ttyS0 (ie, DBGU) */
	at91_set_serial_console(0);


  /* following two uarts are only for Opticell */

	/* UART0 - lcd  on ttyS1 */
	at91_register_uart(AT91SAM9X5_ID_UART0, 1, 0);

	/* UART1 - voltage monitor  on ttyS2 */
	at91_register_uart(AT91SAM9X5_ID_UART1, 2, 0);
}

/*
 * USB Host port (OHCI)
 */
/* Port A is shared with gadget port & Port C is full-speed only */
static struct at91_usbh_data __initdata ek_usbh_fs_data = {
	.ports		= 3,
};

/*
 * USB HS Host port (EHCI)
 */
/* Port A is shared with gadget port */
static struct at91_usbh_data __initdata ek_usbh_hs_data = {
	.ports		= 2,
};


/*
 * USB HS Device port
 */
static struct usba_platform_data __initdata ek_usba_udc_data;


/*
 * MACB Ethernet devices
 */
static struct at91_eth_data __initdata ek_macb0_data = {
	.is_rmii	= 0,
};


/* --------------------------
 * GPIO Buttons
 */
#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)
static struct gpio_keys_button acnbfx_buttons[] = {
	{	/* SW1, "default" */
		.code		= BTN_0,
		.gpio		= AT91_PIN_PD0,
		.active_low	= 0,
		.desc		= "reset_defaults",
		.wakeup		= 1,
	},
};

static struct gpio_keys_platform_data acnbfx_button_data = {
	.buttons	= acnbfx_buttons,
	.nbuttons	= ARRAY_SIZE(acnbfx_buttons),
};

static struct platform_device acnbfx_button_device = {
	.name		= "gpio-keys",
	.id		= -1,
	.num_resources	= 0,
	.dev		= {
		.platform_data	= &acnbfx_button_data,
	}
};

static void __init acnbfx_add_device_buttons(void)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(acnbfx_buttons); i++) {
		at91_set_pulldown(acnbfx_buttons[i].gpio, 0);
		at91_set_gpio_input(acnbfx_buttons[i].gpio, 1);
		at91_set_deglitch(acnbfx_buttons[i].gpio, 1);
	}

	platform_device_register(&acnbfx_button_device);
}
#else
static void __init acnbfx_add_device_buttons(void) {}
#endif


/* --------------------------
 * GPIO LEDs
 */
static struct gpio_led acnbfx_leds[] = {
	{
		.name			= "usb_red",
		.gpio			= AT91_PIN_PC3,
		.active_low		= 1,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_ON,
	},
	{
		.name			= "usb_green",
		.gpio			= AT91_PIN_PC4,
		.active_low		= 1,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "usb_blue",
		.gpio			= AT91_PIN_PC5,
		.active_low		= 1,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "3g_red",
		.gpio			= AT91_PIN_PC6,
		.active_low		= 1,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "3g_green",
		.gpio			= AT91_PIN_PC7,
		.active_low		= 1,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "3g_blue",
		.gpio			= AT91_PIN_PC8,
		.active_low		= 1,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "sig1",
		.gpio			= AT91_PIN_PC9,
		.active_low		= 0,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "sig2",
		.gpio			= AT91_PIN_PC10,
		.active_low		= 0,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "sig3",
		.gpio			= AT91_PIN_PC11,
		.active_low		= 0,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "sig4",
		.gpio			= AT91_PIN_PC12,
		.active_low		= 0,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
	{
		.name			= "sig5",
		.gpio			= AT91_PIN_PC13,
		.active_low		= 0,
		.default_trigger	= "none",
		.default_state		= LEDS_GPIO_DEFSTATE_OFF,
	},
};

static void __init acnbfx_add_device_leds(void)
{
	at91_gpio_leds(acnbfx_leds, ARRAY_SIZE(acnbfx_leds));
}

static struct gpio gpios[] = {
  /* Ethernet Pins */
  {AT91_PIN_PA0,  GPIOF_OUT_INIT_HIGH, "Ethernet Enable"},
  {AT91_PIN_PA1,  GPIOF_IN,            "Ethernet Active"},
  {AT91_PIN_PA2,  GPIOF_IN,            "Ethernet Link"},

  /* USB Power */
  /* Netbridge mappings */
  /* D4 = internel enable
   * D5 = external enable
   * D8 = internel overcurrent sense
   * D9 = external overcurrent sense
   */
  /* opticell mappings
   * D4 = external enable
   * D5 = usb port 1 (bottom, towards lcd) enable
   * D6 = usb port 2 enable
   * D7 = usb port 3 enable
   * D8 = usb port 4 enable
   * D10 = poweroff
   */
  {AT91_PIN_PD4,  GPIOF_OUT_INIT_LOW, "USB Port 0 Enable"},
  {AT91_PIN_PD5,  GPIOF_OUT_INIT_LOW, "USB Port 1 Enable"},
  {AT91_PIN_PD6,  GPIOF_OUT_INIT_LOW, "USB Port 2 Enable"},
  {AT91_PIN_PD7,  GPIOF_OUT_INIT_LOW, "USB Port 3 Enable"},
  {AT91_PIN_PD8,  GPIOF_IN,           "USB Internal Overcurrent"},
  {AT91_PIN_PD9,  GPIOF_IN,           "USB External Overcurrent"},
  {AT91_PIN_PD10, GPIOF_OUT_INIT_HIGH,           "PowerOff"},
};

static void __init acnbfx_export_gpio(void)
{
  int err = gpio_request_array(gpios, ARRAY_SIZE(gpios));
  if (err == 0){
    int i;
    for (i=0; i < ARRAY_SIZE(gpios); i++){
      gpio_export(gpios[i].gpio, 1);
    }
    /* flip eth link and act indicator polarity */
    gpio_sysfs_set_active_low(AT91_PIN_PA2, 1);
    gpio_sysfs_set_active_low(AT91_PIN_PA1, 1);
    /* flip usb enable/disable polarity */
    gpio_sysfs_set_active_low(AT91_PIN_PD4, 1);
    gpio_sysfs_set_active_low(AT91_PIN_PD5, 1);
    gpio_sysfs_set_active_low(AT91_PIN_PD6, 1);
    gpio_sysfs_set_active_low(AT91_PIN_PD7, 1);
    /* flip usb overcurrent indicator polarity */
    gpio_sysfs_set_active_low(AT91_PIN_PD8, 1);
    gpio_sysfs_set_active_low(AT91_PIN_PD9, 1);
    /* flip power indicator polarity */
    gpio_sysfs_set_active_low(AT91_PIN_PD10, 1);
  }
}
void __init nbfx100_init_irq(void)
{
	at91sam9x5_init_interrupts(NULL);
}

/*
 * SPI devices.
 */

//static struct mtd_partition __initdata atmel_spi_flash_partitions[] = {
//  { .name  = "old_loader", .offset = 0                 , .size =  93 * 264, .mask_flags = MTD_WRITEABLE }, // read-only
//  { .name  = "old_u-boot", .offset = MTDPART_OFS_NXTBLK, .size = 416 * 264, .mask_flags = MTD_WRITEABLE }, // read-only
//  { .name  = "old_ub-env", .offset = MTDPART_OFS_NXTBLK, .size =   3 * 264, .mask_flags = 0             },
//};

static struct mtd_partition __initdata jedec_spi_flash_partitions[] = {
  { .name  = "loader",     .offset = 0                 , .size =   3 * 0x10000, .mask_flags = 0 },
  { .name  = "u-boot",     .offset = MTDPART_OFS_NXTBLK, .size =   4 * 0x10000, .mask_flags = 0 },
  { .name  = "u-boot-env", .offset = MTDPART_OFS_NXTBLK, .size =   1 * 0x10000, .mask_flags = 0 },
  { .name  = "kernel0",    .offset = MTDPART_OFS_NXTBLK, .size =  32 * 0x10000, .mask_flags = 0 },
  { .name  = "kernel1",    .offset = MTDPART_OFS_NXTBLK, .size =  32 * 0x10000, .mask_flags = 0 },
  { .name  = "fs0",        .offset = MTDPART_OFS_NXTBLK, .size = 204 * 0x10000, .mask_flags = 0 },
  { .name  = "fs1",        .offset = MTDPART_OFS_NXTBLK, .size = 204 * 0x10000, .mask_flags = 0 },
  { .name  = "config",     .offset = MTDPART_OFS_NXTBLK, .size =  16 * 0x10000, .mask_flags = 0 },
  { .name  = "store",      .offset = MTDPART_OFS_NXTBLK, .size =  16 * 0x10000, .mask_flags = 0 },
};

//static const struct flash_platform_data atmel_spi_flash_data = {
//		.name           = "atmel_flash",
//    .parts    = atmel_spi_flash_partitions,
//    .nr_parts = ARRAY_SIZE(atmel_spi_flash_partitions),
//};

static const struct flash_platform_data jedec_spi_flash_data = {
		.name           = "jedec_flash",
		.type           = "mx25l25635e",
    .parts    = jedec_spi_flash_partitions,
    .nr_parts = ARRAY_SIZE(jedec_spi_flash_partitions),
};

static struct spi_board_info acnbfx_spi_devices[] = {
#if defined(CONFIG_SPI_ATMEL) || defined(CONFIG_SPI_ATMEL_MODULE)
//#if defined(CONFIG_MTD_DATAFLASH)
//	{	/* serial flash chip */
//		.modalias	= "mtd_dataflash",
//		.chip_select	= 0,
//		.max_speed_hz	= 5 * 1000 * 1000,
//		.bus_num	= 0,
//		.mode		= SPI_MODE_0,
//		.platform_data  = &atmel_spi_flash_data,
//	},
//#endif
#if defined(CONFIG_MTD_M25P80)
	{	/* serial flash chip */
		.modalias	= "mx25l25635e",
		.chip_select	= 1,
		.max_speed_hz	= 50 * 1000 * 1000,
		.bus_num	= 0,
		.mode		= SPI_MODE_0,
		.platform_data  = &jedec_spi_flash_data,
	},
#endif
#endif
};

static void __init nbfx100_board_init(void)
{

	/* Serial */
	at91_add_device_serial();
	/* USB HS Host */
	at91_add_device_usbh_ohci(&ek_usbh_fs_data);
	at91_add_device_usbh_ehci(&ek_usbh_hs_data);
	/* USB HS Device */
	at91_add_device_usba(&ek_usba_udc_data);
	/* Ethernet */
	at91_add_device_eth(0, &ek_macb0_data);


	/* Push Buttons */
	acnbfx_add_device_buttons();
  /* LEDs */
  if (machine_is_acfx100nb()) {
    acnbfx_add_device_leds();
  }
  /* SPI FLASH */
	at91_add_device_spi(acnbfx_spi_devices, ARRAY_SIZE(acnbfx_spi_devices));
  /* Generic GPIO for ethernet, usb */
  acnbfx_export_gpio();
}

MACHINE_START(ACFX100NB, "Accelecon ACFX100-Series NetBridge")
	/* Maintainer: Acclerated Concepts */
	.boot_params	= AT91_SDRAM_BASE + 0x100,
	.timer		= &at91sam926x_timer,
	.map_io		= nbfx100_map_io,
	.init_irq	= nbfx100_init_irq,
	.init_machine	= nbfx100_board_init,
MACHINE_END

MACHINE_START(ACFX100OC, "Accelecon ACFX100-Series OptiCell")
	/* Maintainer: Acclerated Concepts */
	.boot_params	= AT91_SDRAM_BASE + 0x100,
	.timer		= &at91sam926x_timer,
	.map_io		= nbfx100_map_io,
	.init_irq	= nbfx100_init_irq,
	.init_machine	= nbfx100_board_init,
MACHINE_END
